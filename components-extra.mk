COMPONENT_DIRS = \
	developer/cmake \
	developer/mercurial \
	gc \
	gnu-iconv \
	libgpg-error \
	library/libarchive \
	library/libassuan \
	library/libgcrypt \
	library/libksba \
	library/npth \
	library/ntbtls \
	libusb/ugen \
	libusb/wrapper \
	mail/postfix \
	network/lftp \
	network/rsync \
	network/tuntap \
	python/pip \
	sysutils/gnupg \
	web/nginx
