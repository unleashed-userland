#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright (c) 2011, 2012, Oracle and/or its affiliates. All rights reserved.
#

include ../../../make-rules/shared-macros.mk

COMPONENT_NAME=		autoconf
COMPONENT_VERSION=	2.59
COMPONENT_PROJECT_URL=	http://www.gnu.org/software/autoconf/
COMPONENT_SRC=		$(COMPONENT_NAME)-$(COMPONENT_VERSION)
COMPONENT_ARCHIVE=	$(COMPONENT_SRC).tar.gz
COMPONENT_ARCHIVE_HASH=	\
    sha256:9cd05c73c5fcb1f5ccae53dd6cac36bb8cb9c7b3e97ffae5a7c05c72594c88d8
COMPONENT_ARCHIVE_URL=	http://ftp.gnu.org/gnu/autoconf/$(COMPONENT_ARCHIVE)
COMPONENT_BUGDB=	utility/autoconf

include $(WS_TOP)/make-rules/prep.mk
include $(WS_TOP)/make-rules/configure.mk
include $(WS_TOP)/make-rules/ips.mk

COMPONENT_PRE_CONFIGURE_ACTION = ($(CLONEY) $(SOURCE_DIR) $(@D))
# avoid help2man dep by considering the manpages up-to-date, even if we updated
# aux files
COMPONENT_POST_CONFIGURE_ACTION = touch $(SOURCE_DIR)/man/*.1
# avoid makeinfo dependency by considering stamp-vti up to date, even if
# ./configure was regenerated
COMPONENT_POST_CONFIGURE_ACTION += ; touch -r $(SOURCE_DIR)/configure $(SOURCE_DIR)/doc/stamp-vti

CONFIGURE_OPTIONS  +=		--srcdir=.
CONFIGURE_OPTIONS  +=		--infodir=$(CONFIGURE_INFODIR)
CONFIGURE_OPTIONS  +=		PERL="$(PERL)"
CONFIGURE_OPTIONS  +=		--program-suffix=-$(COMPONENT_VERSION)
CONFIGURE_BINDIR.64 = $(CONFIGURE_PREFIX)/bin

COMPONENT_BUILD_GMAKE_ARGS += pkgdatadir=$(CONFIGURE_PREFIX)/share/autoconf-$(COMPONENT_VERSION)
COMPONENT_INSTALL_GMAKE_ARGS += pkgdatadir=$(CONFIGURE_PREFIX)/share/autoconf-$(COMPONENT_VERSION)

# common targets
build:		$(BUILD_64)

install:	$(INSTALL_64)

test:		$(TEST_64)

BUILD_PKG_DEPENDENCIES =	$(BUILD_TOOLS)

include $(WS_TOP)/make-rules/depend.mk
