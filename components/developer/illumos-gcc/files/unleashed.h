/* Operating system specific defines to be used when targeting GCC for any
   Unleashed
   Copyright (C) 2002-2014 Free Software Foundation, Inc.

This file is part of GCC.

GCC is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

GCC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GCC; see the file COPYING3.  If not see
<http://www.gnu.org/licenses/>.  */

#define SIG_ATOMIC_TYPE "int"

/* Names to predefine in the preprocessor for this target machine.  */
#define TARGET_OS_CPP_BUILTINS()			\
    do {						\
	builtin_define_std ("unix");			\
	builtin_define_std ("sun");			\
	builtin_define ("__svr4__");			\
	builtin_define ("__SVR4");			\
	builtin_define ("__unleashed__");		\
	builtin_assert ("system=unix");			\
	builtin_assert ("system=svr4");			\
    } while (0)

/* The Solaris linker will not merge a read-only .eh_frame section
   with a read-write .eh_frame section.  None of the encodings used
   with non-PIC code require runtime relocations.  In 64-bit mode,
   since there is no backwards compatibility issue, we use a read-only
   section for .eh_frame.  In 32-bit mode, we use a writable .eh_frame
   section in order to be compatible with G++ for Solaris x86.  */
#undef EH_TABLES_CAN_BE_READ_ONLY
#define EH_TABLES_CAN_BE_READ_ONLY (TARGET_64BIT)

/* Use --as-needed/-z ignore -lgcc_s for eh support.  */
#ifdef HAVE_LD_AS_NEEDED
#define USE_LD_AS_NEEDED 1
#endif

#undef STARTFILE_SPEC
#define STARTFILE_SPEC "%{!shared:%{p|pg:gcrt1.o%s;:crt1.o%s}} " \
	"%{ansi:values-Xc.o%s}%{!ansi:values-Xa.o%s} " \
	"crti.o%s crtbegin.o%s"
#undef ENDFILE_SPEC
#define ENDFILE_SPEC "crtend.o%s crtn.o%s"

/* XXX our headers should be fixed to use the same integral types for these  on both 32 and 64 bit */
#undef WCHAR_TYPE
#define WCHAR_TYPE (TARGET_64BIT ? "int" : "long int")
#undef WCHAR_TYPE_SIZE
#define WCHAR_TYPE_SIZE 32
#undef WINT_TYPE
#define WINT_TYPE (TARGET_64BIT ? "int" : "long int")
#undef WINT_TYPE_SIZE
#define WINT_TYPE_SIZE 32

/* sunld spec */
#define LINK_SPEC \
	"%{h*} %{YP,*} %{R*} %{shared:-G -dy %{!mimpure-text:-z text}} " \
	"%{static:-dn -Bstatic} %{symbolic:-Bsymbolic -G -dy -z text}"

#define STARTFILE_PREFIX_SPEC "%{m32:/lib/i386/ /usr/lib/i386/}%{!m32:/lib/ /lib/amd64/ /usr/lib/ /usr/lib/amd64/}"

#undef LIB_SPEC
#define LIB_SPEC "%{!shared:%{pthread:-lpthread} %{p|pg:-ldl} -lc}"

/* The system headers under Solaris 2 are C++-aware since 2.0.  */
#define NO_IMPLICIT_EXTERN_C

/* Allow macro expansion in #pragma pack.  */
#define HANDLE_PRAGMA_PACK_WITH_EXPANSION 1
#define HANDLE_PRAGMA_PACK_PUSH_POP 1

/* Support Solaris-specific format checking for cmn_err.  */
#define TARGET_N_FORMAT_TYPES 1
#define TARGET_FORMAT_TYPES solaris_format_types
extern GTY(()) tree solaris_pending_aligns;
extern GTY(()) tree solaris_pending_inits;
extern GTY(()) tree solaris_pending_finis;

/* #pragma init and #pragma fini are implemented on top of init and
   fini attributes.  */
#define SOLARIS_ATTRIBUTE_TABLE						\
  { "init",      0, 0, true,  false,  false, NULL },			\
  { "fini",      0, 0, true,  false,  false, NULL }
/* Solaris-specific #pragmas are implemented on top of attributes.  Hook in
   the bits from config/sol2.c.  */
#define SUBTARGET_INSERT_ATTRIBUTES solaris_insert_attributes
#define SUBTARGET_ATTRIBUTE_TABLE SOLARIS_ATTRIBUTE_TABLE

/* Register the Solaris-specific #pragma directives.  */
#define REGISTER_SUBTARGET_PRAGMAS() solaris_register_pragmas ()

/* Output a simple call for .init/.fini.  */
#define ASM_OUTPUT_CALL(FILE, FN)				\
  do								\
    {								\
      fprintf (FILE, "\tcall\t");				\
      print_operand (FILE, XEXP (DECL_RTL (FN), 0), 'P');	\
      fprintf (FILE, "\n");					\
    }								\
  while (0)

#define SUBTARGET_OVERRIDE_OPTIONS				\
  do								\
    {								\
      if (flag_omit_frame_pointer == 2)				\
	flag_omit_frame_pointer = 0;				\
    }								\
  while (0)

#undef TARGET_SUBTARGET_DEFAULT
#define TARGET_SUBTARGET_DEFAULT (MASK_80387 | MASK_IEEE_FP	\
				  | MASK_FLOAT_RETURNS)

#define SUBTARGET_OPTIMIZATION_OPTIONS			\
  do							\
    {							\
      if (optimize >= 1)				\
	target_flags |= MASK_OMIT_LEAF_FRAME_POINTER;	\
    }							\
  while (0)
#undef SUBTARGET_RETURN_IN_MEMORY
#define SUBTARGET_RETURN_IN_MEMORY(TYPE, FNTYPE) \
	ix86_sol10_return_in_memory (TYPE, FNTYPE)

/* This is how to declare the size of a function.  For Solaris, we output
   any .init or .fini entries here.  */
#undef ASM_DECLARE_FUNCTION_SIZE
#define ASM_DECLARE_FUNCTION_SIZE(FILE, FNAME, DECL)		\
  do								\
    {								\
      if (!flag_inhibit_size_directive)				\
	ASM_OUTPUT_MEASURED_SIZE (FILE, FNAME);			\
      solaris_output_init_fini (FILE, DECL);			\
    }								\
  while (0)

/* Only recent versions of Solaris 11 ld properly support hidden .gnu.linkonce
   sections, so don't use them.  */
#define USE_HIDDEN_LINKONCE 0

#undef SWITCH_TAKES_ARG
#define SWITCH_TAKES_ARG(CHAR) \
  (DEFAULT_SWITCH_TAKES_ARG(CHAR) \
   || (CHAR) == 'R' \
   || (CHAR) == 'h' \
   || (CHAR) == 'z')
