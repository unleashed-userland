/* Operating system specific defines to be used when targeting GCC for any
   Unleashed
   Copyright (C) 2002-2014 Free Software Foundation, Inc.

This file is part of GCC.

GCC is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

GCC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GCC; see the file COPYING3.  If not see
<http://www.gnu.org/licenses/>.  */

#define SIG_ATOMIC_TYPE "int"

/* Names to predefine in the preprocessor for this target machine.  */
#define TARGET_OS_CPP_BUILTINS()			\
    do {						\
	builtin_define_std ("unix");			\
	builtin_define ("__unleashed__");		\
	builtin_assert ("system=unix");			\
	builtin_assert ("system=svr4");			\
    } while (0)

/* Use --as-needed/-z ignore -lgcc_s for eh support.  */
#ifdef HAVE_LD_AS_NEEDED
#define USE_LD_AS_NEEDED 1
#endif

#undef STARTFILE_SPEC
#define STARTFILE_SPEC "%{!shared:%{p|pg:gcrt1.o%s;:crt1.o%s}} " \
	"crti.o%s %{shared|pie:crtbeginS.o%s;:crtbegin.o%s}"
#undef ENDFILE_SPEC
#define ENDFILE_SPEC "%{shared|pie:crtendS.o%s;:crtend.o%s} crtn.o%s"

/* XXX our headers should be fixed to use the same integral types for these  on both 32 and 64 bit */
#undef WCHAR_TYPE
#define WCHAR_TYPE (TARGET_64BIT ? "int" : "long int")
#undef WCHAR_TYPE_SIZE
#define WCHAR_TYPE_SIZE 32
#undef WINT_TYPE
#define WINT_TYPE (TARGET_64BIT ? "int" : "long int")
#undef WINT_TYPE_SIZE
#define WINT_TYPE_SIZE 32

/* elf_i386 format uses /usr/lib/libc.so.1 as interpreter; elf_i386_ldso would
 * use ld.so.1, but so does elf_i386_sol2, so let's use that for now. */
#define LINK_SPEC "%{m32:-m elf_i386_sol2 -L/lib/i386 -L/lib -L/usr/lib/i386 -L/usr/lib}%{!m32:-m elf_x86_64}" \
	"%{h*} %{R*} %{shared} %{static} %{symbolic:-Bsymbolic} " \
	"%{!shared:%{static:%{rdynamic:--export-dynamic}}}"

#define STARTFILE_PREFIX_SPEC "%{m32:/lib/i386/ /usr/lib/i386/}%{!m32:/lib/ /lib/amd64/ /usr/lib/ /usr/lib/amd64/}"

#undef LIB_SPEC
#define LIB_SPEC "%{!shared:%{pthread:-lpthread} %{p|pg:-ldl} -lc}"

#define LINK_EH_SPEC "%{!static:--eh-frame-hdr} "

/* The system headers under Solaris 2 are C++-aware since 2.0.  */
#define NO_IMPLICIT_EXTERN_C

/* Allow macro expansion in #pragma pack.  */
#define HANDLE_PRAGMA_PACK_WITH_EXPANSION

#define USE_PT_GNU_EH_FRAME
