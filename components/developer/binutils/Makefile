#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright (c) 2011, 2013, Oracle and/or its affiliates. All rights reserved.
#
include ../../../make-rules/shared-macros.mk

COMPONENT_NAME=		binutils
COMPONENT_VERSION=	2.32
COMPONENT_PROJECT_URL=	http://www.gnu.org/software/binutils/
COMPONENT_SRC=		$(COMPONENT_NAME)-$(COMPONENT_VERSION)
COMPONENT_ARCHIVE=	$(COMPONENT_SRC).tar.xz
COMPONENT_ARCHIVE_URL=	https://ftp.gnu.org/gnu/$(COMPONENT_NAME)/$(COMPONENT_ARCHIVE)
COMPONENT_ARCHIVE_HASH=	\
	sha256:0ab6c55dd86a92ed561972ba15b9b70a8b9f75557f896446c82e8b36e473ee04
COMPONENT_BUGDB=	utility/gnu-binutils

include $(WS_TOP)/make-rules/prep.mk
include $(WS_TOP)/make-rules/configure.mk
include $(WS_TOP)/make-rules/ips.mk

PATH=/usr/gnu/bin:/usr/bin

GNU_ARCH :=	$(shell /usr/share/automake-1.15/config.sub $$(/usr/share/automake-1.15/config.guess))
PKG_MACROS +=	GNU_ARCH="$(GNU_ARCH)"

CFLAGS += -std=c99

CONFIGURE_BINDIR.64 =	$(CONFIGURE_PREFIX)/bin

CONFIGURE_OPTIONS +=	--mandir=$(CONFIGURE_MANDIR)
CONFIGURE_OPTIONS +=	--infodir=$(CONFIGURE_INFODIR)
# XXX: our linker does not understand these relocs, so don't generate them.
# this could change if/when the linker changes.
CONFIGURE_OPTIONS +=	--enable-x86-relax-relocations=no

COMPONENT_BUILD_GMAKE_ARGS += tooldir=$(CONFIGURE_PREFIX)
COMPONENT_INSTALL_GMAKE_ARGS += tooldir=$(CONFIGURE_PREFIX)

# common targets
build:		$(BUILD_64)

install:	$(INSTALL_64)

test: $(TEST_64)

REQUIRED_PACKAGES += library/zlib
REQUIRED_PACKAGES += system/library
