#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright (c) 2010, 2015, Oracle and/or its affiliates. All rights reserved.
#

GPATCH =	/usr/gnu/bin/patch
PATCH_LEVEL ?=	1
GPATCH_BACKUP =	--backup --version-control=numbered
GPATCH_FLAGS =	--strip=$(PATCH_LEVEL) $(GPATCH_BACKUP)

#
# Rules for patching source that is downloaded and unpacked or pulled from
# a source repository.
#
# PATCH_DIR can be overridden to move patches to a different location
# PATCH_PATTERN can be overridden to adjust the patch naming scheme that the
#     build recognizes.
# EXTRA_PATCHES{version} can be defined in the component Makefile to include
#     additional patches.
#

PATCH_PATTERN ?=	*.patch

PATCH_DIR ?=		patches
# patches specific to parfait builds.
ifeq   ($(strip $(PARFAIT_BUILD)),yes)
PARFAIT_PATCH_DIR =	parfait
endif

PATCHES =	$(shell find $(PATCH_DIR) $(PARFAIT_PATCH_DIR) -type f \
			 -name '$(PATCH_PATTERN)' 2>/dev/null | \
				LC_COLLATE=C sort)

$(SOURCE_DIR)/.patched: unpack
	@for p in $(PATCHES); do \
	    b=$$(basename $$p);\
	    if ! [ -f $@-$$b ]; then \
	        echo "$(GPATCH) -d $(@D) $(GPATCH_FLAGS) < $$p";\
	        $(GPATCH) -d $(@D) $(GPATCH_FLAGS) < $$p || exit 1;\
	        touch $@-$$b;\
	    fi;\
	done; touch $@

patch:: $(SOURCE_DIR)/.patched

REQUIRED_PACKAGES += text/gnu-patch
