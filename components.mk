# Note: gcc's are listed early on to give them more time to run.  Otherwise,
# this list is alphabetical.
COMPONENT_DIRS = \
	developer/gcc-6 \
	\
	archiver/bzip2 \
	archiver/gtar \
	archiver/gzip \
	archiver/xz \
	autoconf/autoconf \
	autoconf/autoconf-2.59 \
	autoconf/autoconf-2.64 \
	database/berkeleydb \
	database/sqlite \
	developer/autoconf-archive \
	developer/automake-1.9 \
	developer/automake-1.11 \
	developer/automake-1.15 \
	developer/binutils \
	developer/bison \
	developer/gdb \
	developer/git \
	developer/illumos-gcc \
	developer/libtool \
	developer/m4 \
	developer/make \
	editor/vim \
	findutils \
	flex \
	gdbm \
	libffi \
	library/dbus \
	library/dbus-glib \
	library/gamin \
	library/glib \
	library/gmp \
	library/libevent2 \
	library/libexpat \
	library/libidn \
	library/libxml2 \
	library/mozilla-nspr \
	library/mozilla-nss \
	library/mpfr \
	library/pcre \
	library/readline \
	library/zlib \
	libsigsegv \
	libxslt \
	metaauto \
	meta-packages/history \
	network/openssh \
	nghttp2 \
	openindiana/pkg \
	openntpd \
	perl/perl-522 \
	perl/xml-parser \
	pkgconf \
	python/asn1crypto \
	python/cffi \
	python/cherrypy \
	python/cryptography \
	python/enum34 \
	python/idna \
	python/ipaddress \
	python/jsonrpclib \
	python/jsonschema \
	python/mako \
	python/ply \
	python/pycparser \
	python/pycurl \
	python/pyopenssl \
	python/python27 \
	python/setuptools \
	python/simplejson \
	python/six \
	shell/bash \
	shell/zsh \
	sysutils/cdrtools \
	sysutils/pv \
	sysutils/screen \
	sysutils/sudo \
	sysutils/tmux \
	text/file \
	text/gawk \
	text/gettext \
	text/ggrep \
	text/patch \
	web/curl \
	web/wget \
	x11/makedepend \
	x11/xproto
